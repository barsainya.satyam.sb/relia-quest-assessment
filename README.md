# Employee Application

Welcome to the Employee Application! This application provides CRUD operations for managing employees, and you can explore the APIs using Swagger UI.

## How to Run

To run the Employee application locally, follow these steps:

1. **Clone the repository:**

   ```
   git clone https://gitlab.com/barsainya.satyam.sb/relia-quest-assessment.git
   cd employeeaap
   ```
   
2. **Build the application:**
   
   ```
   ./mvnw clean package
   ```
3. **Run the application:**

   ```
   java -jar target/employeeaap-0.0.1-SNAPSHOT.jar
   ```

4. **Access the application:**
   
   ```
   The application will start on http://localhost:8080/. You can now use the following APIs:
   ```

***APIs***
   
   1. Retrieve all employees
      
   ```
   Method: GET
   URL: /employees
   Description: Retrieves a list of all employees.
   Response:
   [
        {
            "id": "1",
            "employee_name": "John Doe",
            "employee_salary": 520144,
            "employee_age": 42,
            "profile_image": ""
        },
       {
            "id": "2",
            "employee_name": "Jane Smith",
            "employee_salary": 520144,
            "employee_age": 42,
            "profile_image": ""
        }
    ]
   ```

2.  Retrieve an employee by ID

   ```
   Method: GET
   URL: /employees/{id}
   Description: Retrieves an employee by their ID.
   Parameters:
        id: String (required) - The ID of the employee to retrieve.
   
   Response:
        {
            "id": "1",
            "employee_name": "John Doe",
            "employee_salary": 520144,
            "employee_age": 42,
            "profile_image": ""
        }
   ```

3.  Create a new employee

   ```
   Method: POST
   URL: /employees
   Description: Creates a new employee.
   Request Body:
   {
      "id": "3",
      "employee_name": "Alice Johnson",
      "employee_salary": 520144,
      "employee_age": 42,
      "profile_image": ""
    }

   Response:
    {
       "id": "3",
       "employee_name": "Alice Johnson",
       "employee_salary": 520144,
       "employee_age": 42,
       "profile_image": ""
    }

   ```

4.  Search employees by employee_name

   ```
   Method: GET
   URL: /employees/search/{searchString}
   Description: Searches for employees by employee_name using a search string.
   Parameters:
    searchString (path parameter): String - The string to search for in employee employee_names.

   Response:
    {
       "id": "3",
       "employee_name": "Alice Johnson",
       "employee_salary": 520144,
       "employee_age": 42,
       "profile_image": ""
    }

   ```

5. Retrieve the highest salary among employees

   ```
   Method: GET
   URL: /employees/highestSalary
   Description: Retrieves the highest salary among all employees.
   Response:
    150000

   ```

6. Retrieve the employee_names of the top ten highest-earning employees

   ```
   Method: GET
   URL: /employees/topTenHighestEarningEmployeeemployee_names
   Description: Retrieves the employee_names of the top ten employees with the highest earnings.
 
   Response:
    [
        "John Doe",
        "Jane Smith",
        "Alice Johnson",
    ]
   ```

7. Delete an employee by ID

   ```
   Method: DELETE
   URL: /employees/{id}
   Description: Deletes an employee by their ID.
   Parameters:
    id (path parameter): String - The ID of the employee to delete.

   Response:
    Alice Johnson
    
   ```


## Swagger UI
Explore and test the Employee APIs using Swagger UI:

``Swagger UI: http://localhost:8080/swagger-ui/index.html#``