package com.example.employeeaap.service.impl;

import com.example.employeeaap.exception.EmployeeNotFoundException;
import com.example.employeeaap.model.Employee;
import com.example.employeeaap.repository.EmployeeRepository;
import com.example.employeeaap.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service implementation for managing employees.
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    private EmployeeRepository employeeRepository;

    /**
     * Retrieves all employees.
     *
     * @return List of all employees
     */
    @Override
    public List<Employee> getAllEmployees() {
        logger.info("Retrieving all employees.");
        List<Employee> employees = employeeRepository.getAllEmployees();
        logger.debug("Retrieved {} employees.", employees.size());
        return employees;
    }

    /**
     * Retrieves employees by searching their names.
     *
     * @param name The name to search for
     * @return List of employees whose names contain the specified search string
     */
    @Override
    public List<Employee> getEmployeesByNameSearch(String name) {
        logger.info("Searching employees by name: {}", name);
        List<Employee> employees = getAllEmployees().stream()
                .filter(employee -> employee.getEmployee_name().toLowerCase().contains(name.toLowerCase()))
                .collect(Collectors.toList());
        logger.debug("Found {} employees matching the search string: {}", employees.size(), name);
        return employees;
    }

    /**
     * Retrieves an employee by ID.
     *
     * @param id The ID of the employee to retrieve
     * @return The employee object if found
     * @throws EmployeeNotFoundException If no employee with the specified ID exists
     */
    @Override
    public Employee getEmployeeById(String id) {
        logger.info("Retrieving employee with ID: {}", id);
        Employee employee = employeeRepository.getEmployeeById(id)
                .orElseThrow(() -> {
                    logger.error("Employee with ID: {} not found.", id);
                    return new EmployeeNotFoundException("Employee not found");
                });
        logger.debug("Employee retrieved: {}", employee);
        return employee;
    }

    /**
     * Retrieves the highest salary among all employees.
     *
     * @return The highest salary
     */
    @Override
    public int getHighestSalaryOfEmployees() {
        logger.info("Retrieving the highest salary among employees.");
        int highestSalary = getAllEmployees().stream()
                .mapToInt(emp -> {
                    int salary = Integer.parseInt(emp.getEmployee_salary());
                    logger.debug("Employee: {}, Salary: {}", emp.getEmployee_name(), salary);
                    return salary;
                })
                .max().orElse(0);
        logger.debug("Highest salary retrieved: {}", highestSalary);
        return highestSalary;
    }

    /**
     * Retrieves the names of the top 10 employees with the highest earnings.
     *
     * @return List of names of top earning employees
     */
    @Override
    public List<String> getTop10HighestEarningEmployeeNames() {
        logger.info("Retrieving the names of the top 10 highest earning employees.");
        List<String> topEarners = getAllEmployees().stream()
                .sorted(Comparator.comparingInt(emp -> -Integer.parseInt(emp.getEmployee_salary())))
                .limit(10)
                .map(Employee::getEmployee_name)
                .collect(Collectors.toList());
        logger.debug("Top 10 highest earning employees: {}", topEarners);
        return topEarners;
    }

    /**
     * Creates a new employee.
     *
     * @param employee The employee object to create
     * @return The created employee object
     */
    @Override
    public Employee createEmployee(Employee employee) {
        logger.info("Creating a new employee: {}", employee);
        Employee createdEmployee = employeeRepository.createEmployee(employee);
        logger.debug("Employee created successfully: {}", createdEmployee);
        return createdEmployee;
    }

    /**
     * Deletes an employee by ID.
     *
     * @param id The ID of the employee to delete
     * @return The name of the deleted employee
     * @throws EmployeeNotFoundException If no employee with the specified ID exists
     */
    @Override
    public String deleteEmployee(String id) {
        logger.info("Deleting employee with ID: {}", id);
        Employee employee = getEmployeeById(id); // Will log and throw if not found
        employeeRepository.deleteEmployee(id);
        logger.debug("Employee deleted successfully: {}", employee.getEmployee_name());
        return employee.getEmployee_name();
    }
}
