package com.example.employeeaap.service;

import com.example.employeeaap.model.Employee;

import java.util.List;

/**
 * Service interface defining operations for managing employees.
 */
public interface EmployeeService {

    /**
     * Retrieve all employees.
     *
     * @return List of all employees
     */
    List<Employee> getAllEmployees();

    /**
     * Search employees by name.
     *
     * @param name The name to search for
     * @return List of employees matching the search criteria
     */
    List<Employee> getEmployeesByNameSearch(String name);

    /**
     * Retrieve an employee by ID.
     *
     * @param id The ID of the employee to retrieve
     * @return The employee object if found, otherwise null
     */
    Employee getEmployeeById(String id);

    /**
     * Retrieve the highest salary among employees.
     *
     * @return The highest salary
     */
    int getHighestSalaryOfEmployees();

    /**
     * Retrieve the names of the top ten employees with the highest earnings.
     *
     * @return List of employee names
     */
    List<String> getTop10HighestEarningEmployeeNames();

    /**
     * Create a new employee.
     *
     * @param employee The employee object to create
     * @return The created employee object
     */
    Employee createEmployee(Employee employee);

    /**
     * Delete an employee by ID.
     *
     * @param id The ID of the employee to delete
     * @return The name of the deleted employee
     */
    String deleteEmployee(String id);
}
