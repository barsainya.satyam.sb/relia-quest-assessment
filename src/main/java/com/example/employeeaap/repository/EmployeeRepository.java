package com.example.employeeaap.repository;

import com.example.employeeaap.model.Employee;

import java.util.List;
import java.util.Optional;

/**
 * Repository interface for managing employees.
 */
public interface EmployeeRepository {

    /**
     * Retrieves all employees.
     *
     * @return List of all employees
     */
    List<Employee> getAllEmployees();

    /**
     * Retrieves an employee by ID.
     *
     * @param id The ID of the employee to retrieve
     * @return Optional containing the employee if found, otherwise empty
     */
    Optional<Employee> getEmployeeById(String id);

    /**
     * Creates a new employee.
     *
     * @param employee The employee object to create
     * @return The created employee object
     */
    Employee createEmployee(Employee employee);

    /**
     * Deletes an employee by ID.
     *
     * @param id The ID of the employee to delete
     */
    void deleteEmployee(String id);
}
