package com.example.employeeaap.repository.impl;

import com.example.employeeaap.client.EmployeeClient;
import com.example.employeeaap.model.Employee;
import com.example.employeeaap.repository.EmployeeRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Implementation of {@link EmployeeRepository} that interacts with a mock data source.
 */
@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeRepositoryImpl.class);

    @Autowired
    private EmployeeClient employeeClient;

    @Autowired
    private ObjectMapper objectMapper;

    public List<Employee> mockedEmployees;

    /**
     * Initializes the repository by loading mocked employee data from a JSON file.
     */
    @PostConstruct
    public void init() {
        logger.info("Initializing EmployeeRepositoryImpl.");
        mockedEmployees = fetchMockedData("/response/mocked-employees.json");
        logger.info("Loaded {} mocked employees.", mockedEmployees != null ? mockedEmployees.size() : mockedEmployees);
    }

    /**
     * Retrieves all employees.
     *
     * @return List of all employees
     */
    @Override
    public List<Employee> getAllEmployees() {
        logger.info("Returning all mocked employees.");
        // Commented out the client call to return mocked data only
        // try {
        //     List<Employee> employees = employeeClient.getAllEmployees();
        //     logger.debug("Retrieved {} employees from the API.", employees.size());
        //     return employees;
        // } catch (Exception e) {
        //     logger.error("Failed to retrieve employees from the API. Fallback to mocked data.", e);
        //     return mockedEmployees;
        // }
        return mockedEmployees;
    }

    /**
     * Retrieves an employee by ID.
     *
     * @param id The ID of the employee to retrieve
     * @return Optional containing the employee if found, otherwise empty
     */
    @Override
    public Optional<Employee> getEmployeeById(String id) {
        logger.info("Retrieving mocked employee with ID: {}", id);
        // Commented out the client call to return mocked data only
        // try {
        //     Employee employee = employeeClient.getEmployeeById(id);
        //     logger.debug("Employee retrieved: {}", employee);
        //     return Optional.ofNullable(employee);
        // } catch (Exception e) {
        //     logger.error("Failed to retrieve employee with ID: {} from the API. Fallback to mocked data.", id, e);
        //     return mockedEmployees.stream()
        //             .filter(emp -> emp.getId().equals(id))
        //             .findFirst();
        // }
        Optional<Employee> employee = Optional.empty();
        if (mockedEmployees != null && !mockedEmployees.isEmpty()) {
            employee = mockedEmployees.stream()
                    .filter(emp -> emp.getId().equals(id))
                    .findFirst();
        }
        return employee;
    }

    /**
     * Creates a new employee.
     *
     * @param employee The employee object to create
     * @return The created employee object
     */
    @Override
    public Employee createEmployee(Employee employee) {
        logger.info("Creating a new mocked employee: {}", employee);
        // Commented out the client call to return mocked data only
        // try {
        //     Employee createdEmployee = employeeClient.createEmployee(employee);
        //     logger.debug("Employee created successfully: {}", createdEmployee);
        //     return createdEmployee;
        // } catch (Exception e) {
        //     logger.error("Failed to create employee via API. Adding to mocked data.", e);
        //     mockedEmployees.add(employee);
        //     logger.info("Employee added to mocked data: {}", employee);
        //     return employee;
        // }
        if (mockedEmployees != null && !mockedEmployees.isEmpty()) {
            mockedEmployees.add(employee);
        }
        logger.info("Employee added to mocked data: {}", employee);
        return employee;
    }

    /**
     * Deletes an employee by ID.
     *
     * @param id The ID of the employee to delete
     */
    @Override
    public void deleteEmployee(String id) {
        logger.info("Deleting mocked employee with ID: {}", id);
        // Commented out the client call to return mocked data only
        // try {
        //     employeeClient.deleteEmployeeById(id);
        //     logger.debug("Employee with ID: {} deleted successfully from the API.", id);
        // } catch (Exception e) {
        //     logger.error("Failed to delete employee with ID: {} from the API. Removing from mocked data.", id, e);
        //     mockedEmployees.removeIf(emp -> emp.getId().equals(id));
        //     logger.info("Employee with ID: {} removed from mocked data.", id);
        // }
        if (mockedEmployees != null && !mockedEmployees.isEmpty()){
            mockedEmployees.removeIf(emp -> emp.getId().equals(id));
        }
        logger.info("Employee with ID: {} removed from mocked data.", id);
    }

    /**
     * Method to fetch mocked data from JSON file.
     *
     * @param mockedDataLocation The location of the mocked data JSON file
     * @return List of employees read from the JSON file
     */
    public List<Employee> fetchMockedData(String mockedDataLocation) {
        logger.info("Fetching mocked data from file: {}", mockedDataLocation);
        try (InputStream inputStream = TypeReference.class.getResourceAsStream(mockedDataLocation)) {
            if (inputStream != null) {
                List<Employee> employees = objectMapper.readValue(inputStream, new TypeReference<List<Employee>>() {
                });
                logger.debug("Successfully read {} employees from mocked data file.", employees != null ? employees.size() : employees);
                return employees;
            } else {
                logger.error("Failed to read mocked data from file: {}", mockedDataLocation);
                return Collections.emptyList(); // Return empty list on failure
            }
        } catch (IOException e) {
            logger.error("Failed to read mocked data from file: {}", mockedDataLocation, e);
            return Collections.emptyList(); // Return empty list on failure
        }
    }
}
