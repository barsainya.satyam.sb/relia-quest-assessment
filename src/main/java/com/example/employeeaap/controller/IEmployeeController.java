package com.example.employeeaap.controller;

import com.example.employeeaap.model.Employee;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Interface defining RESTful API endpoints for managing employees.
 */
@RestController
public interface IEmployeeController {

    /**
     * Retrieve all employees.
     *
     * @return ResponseEntity with a list of all employees and HTTP status 200 (OK)
     * @throws IOException if there is an error in retrieving the employees
     */
    @GetMapping()
    ResponseEntity<List<Employee>> getAllEmployees() throws IOException;

    /**
     * Search employees by name using a search string.
     *
     * @param searchString The string to search for in employee names
     * @return ResponseEntity with a list of employees matching the search criteria and HTTP status 200 (OK)
     */
    @GetMapping("/search/{searchString}")
    ResponseEntity<List<Employee>> getEmployeesByNameSearch(@PathVariable String searchString);

    /**
     * Retrieve an employee by ID.
     *
     * @param id The ID of the employee to retrieve
     * @return ResponseEntity with the employee object and HTTP status 200 (OK)
     */
    @GetMapping("/{id}")
    ResponseEntity<Employee> getEmployeeById(@PathVariable String id);

    /**
     * Retrieve the highest salary among employees.
     *
     * @return ResponseEntity with the highest salary and HTTP status 200 (OK)
     */
    @GetMapping("/highestSalary")
    ResponseEntity<Integer> getHighestSalaryOfEmployees();

    /**
     * Retrieve the names of the top ten employees with the highest earnings.
     *
     * @return ResponseEntity with a list of employee names and HTTP status 200 (OK)
     */
    @GetMapping("/topTenHighestEarningEmployeeNames")
    ResponseEntity<List<String>> getTopTenHighestEarningEmployeeNames();

    /**
     * Create a new employee.
     *
     * @param employeeInput The request body containing employee details
     * @return ResponseEntity with the created employee object and HTTP status 200 (OK)
     */
    @PostMapping()
    ResponseEntity<Employee> createEmployee(@RequestBody Map<String, Object> employeeInput);

    /**
     * Delete an employee by ID.
     *
     * @param id The ID of the employee to delete
     * @return ResponseEntity with the name of the deleted employee and HTTP status 200 (OK)
     */
    @DeleteMapping("/{id}")
    ResponseEntity<String> deleteEmployeeById(@PathVariable String id);

}
