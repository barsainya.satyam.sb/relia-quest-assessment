package com.example.employeeaap.controller.impl;

import com.example.employeeaap.controller.IEmployeeController;
import com.example.employeeaap.model.Employee;
import com.example.employeeaap.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Implementation of the RESTful API endpoints for managing employees.
 */
@RestController
@RequestMapping("/api/employees")
public class EmployeeControllerImpl implements IEmployeeController {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeControllerImpl.class);

    @Autowired
    private EmployeeService employeeService;

    /**
     * Retrieve all employees.
     *
     * @return ResponseEntity with a list of all employees and HTTP status 200 (OK)
     * @throws IOException if there is an error in retrieving the employees
     */
    @Override
    public ResponseEntity<List<Employee>> getAllEmployees() throws IOException {
        logger.info("Fetching all employees.");
        try {
            List<Employee> employees = employeeService.getAllEmployees();
            logger.debug("Retrieved {} employees.", employees.size());
            return ResponseEntity.ok(employees);
        } catch (Exception e) {
            logger.error("Error fetching all employees", e);
            throw e; // rethrow the exception after logging it
        }
    }

    /**
     * Search employees by name using a search string.
     *
     * @param searchString The string to search for in employee names
     * @return ResponseEntity with a list of employees matching the search criteria and HTTP status 200 (OK)
     */
    @Override
    public ResponseEntity<List<Employee>> getEmployeesByNameSearch(@PathVariable String searchString) {
        logger.info("Searching employees by name: {}", searchString);
        try {
            List<Employee> employees = employeeService.getEmployeesByNameSearch(searchString);
            logger.debug("Found {} employees matching the search criteria.", employees.size());
            return ResponseEntity.ok(employees);
        } catch (Exception e) {
            logger.error("Error searching employees by name: {}", searchString, e);
            throw e; // rethrow the exception after logging it
        }
    }

    /**
     * Retrieve an employee by ID.
     *
     * @param id The ID of the employee to retrieve
     * @return ResponseEntity with the employee object and HTTP status 200 (OK)
     */
    @Override
    public ResponseEntity<Employee> getEmployeeById(@PathVariable String id) {
        logger.info("Fetching employee with ID: {}", id);
        try {
            Employee employee = employeeService.getEmployeeById(id);
            logger.debug("Employee found: {}", employee);
            return ResponseEntity.ok(employee);
        } catch (Exception e) {
            logger.error("Error fetching employee with ID: {}", id, e);
            throw e; // rethrow the exception after logging it
        }
    }

    /**
     * Retrieve the highest salary among employees.
     *
     * @return ResponseEntity with the highest salary and HTTP status 200 (OK)
     */
    @Override
    public ResponseEntity<Integer> getHighestSalaryOfEmployees() {
        logger.info("Fetching the highest salary among employees.");
        try {
            int highestSalary = employeeService.getHighestSalaryOfEmployees();
            logger.debug("Highest salary found: {}", highestSalary);
            return ResponseEntity.ok(highestSalary);
        } catch (Exception e) {
            logger.error("Error fetching the highest salary", e);
            throw e; // rethrow the exception after logging it
        }
    }

    /**
     * Retrieve the names of the top ten employees with the highest earnings.
     *
     * @return ResponseEntity with a list of employee names and HTTP status 200 (OK)
     */
    @Override
    public ResponseEntity<List<String>> getTopTenHighestEarningEmployeeNames() {
        logger.info("Fetching names of the top ten highest earning employees.");
        try {
            List<String> topEarners = employeeService.getTop10HighestEarningEmployeeNames();
            logger.debug("Top 10 highest earning employees found: {}", topEarners);
            return ResponseEntity.ok(topEarners);
        } catch (Exception e) {
            logger.error("Error fetching the top ten highest earning employees", e);
            throw e; // rethrow the exception after logging it
        }
    }

    /**
     * Create a new employee.
     *
     * @param employeeInput The request body containing employee details
     * @return ResponseEntity with the created employee object and HTTP status 200 (OK)
     */
    @Override
    public ResponseEntity<Employee> createEmployee(@RequestBody Map<String, Object> employeeInput) {
        logger.info("Creating a new employee with input: {}", employeeInput);
        try {
            String name = (String) employeeInput.get("employee_name");
            String salary = String.valueOf(employeeInput.get("employee_salary"));
            String age = String.valueOf(employeeInput.get("employee_age"));
            String profile = (String) employeeInput.get("profile_image");

            // Create the Employee object
            Employee employee = new Employee();
            employee.setId(String.valueOf(UUID.randomUUID())); // Assuming ID generation
            employee.setEmployee_name(name);
            employee.setEmployee_salary(salary);
            employee.setEmployee_age(age);
            employee.setProfile_image(profile);

            // Call the service to create the employee
            Employee createdEmployee = employeeService.createEmployee(employee);

            logger.debug("Employee created successfully: {}", createdEmployee);
            return ResponseEntity.ok(createdEmployee);
        } catch (Exception e) {
            logger.error("Error creating employee with input: {}", employeeInput, e);
            throw e; // rethrow the exception after logging it
        }
    }

    /**
     * Delete an employee by ID.
     *
     * @param id The ID of the employee to delete
     * @return ResponseEntity with the name of the deleted employee and HTTP status 200 (OK)
     */
    @Override
    public ResponseEntity<String> deleteEmployeeById(@PathVariable String id) {
        logger.info("Deleting employee with ID: {}", id);
        try {
            String deletedEmployeeName = employeeService.deleteEmployee(id);
            logger.debug("Employee deleted successfully: {}", deletedEmployeeName);
            return ResponseEntity.ok(deletedEmployeeName);
        } catch (Exception e) {
            logger.error("Error deleting employee with ID: {}", id, e);
            throw e; // rethrow the exception after logging it
        }
    }
}
