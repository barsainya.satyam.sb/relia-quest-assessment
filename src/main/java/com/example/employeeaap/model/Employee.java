package com.example.employeeaap.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    private String id;
    private String employee_name;
    private String employee_salary;
    private String employee_age;
    private String profile_image;

    // Getters and Setters
    // toString, equals, and hashCode methods
}
