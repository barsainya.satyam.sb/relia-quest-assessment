package com.example.employeeaap.client;

import com.example.employeeaap.model.Employee;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Feign client interface for interacting with the Employee API.
 */
@FeignClient(name = "employeeClient", url = "${feign.client.url}")
public interface EmployeeClient {

    /**
     * Retrieves all employees.
     *
     * @return List of all employees
     */
    @GetMapping("/employees")
    List<Employee> getAllEmployees();

    /**
     * Retrieves an employee by ID.
     *
     * @param id The ID of the employee to retrieve
     * @return The employee object corresponding to the given ID
     */
    @GetMapping("/employee/{id}")
    Employee getEmployeeById(@PathVariable("id") String id);

    /**
     * Creates a new employee.
     *
     * @param employee The employee object to create
     * @return The created employee object
     */
    @PostMapping("/create")
    Employee createEmployee(@RequestBody Employee employee);

    /**
     * Deletes an employee by ID.
     *
     * @param id The ID of the employee to delete
     */
    @DeleteMapping("/delete/{id}")
    void deleteEmployeeById(@PathVariable("id") String id);
}
