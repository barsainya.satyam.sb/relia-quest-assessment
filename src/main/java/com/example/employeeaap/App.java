package com.example.employeeaap;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

/**
 * Main application class for Employee Application.
 */
@SpringBootApplication
@EnableFeignClients
public class App {

    /**
     * Main method to start the Spring Boot application.
     *
     * @param args Command-line arguments passed to the application
     */
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    /**
     * Creates and configures an ObjectMapper bean.
     *
     * @return ObjectMapper bean instance
     */
    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

}
