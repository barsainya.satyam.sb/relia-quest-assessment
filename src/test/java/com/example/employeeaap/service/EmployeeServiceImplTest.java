package com.example.employeeaap.service;

import com.example.employeeaap.exception.EmployeeNotFoundException;
import com.example.employeeaap.model.Employee;
import com.example.employeeaap.repository.EmployeeRepository;
import com.example.employeeaap.service.impl.EmployeeServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class EmployeeServiceImplTest {

    @Mock
    private EmployeeRepository employeeRepository;

    @InjectMocks
    private EmployeeServiceImpl employeeService;

    private Employee mockEmployee;

    @BeforeEach
    void setUp() {
        mockEmployee = new Employee();
        mockEmployee.setId("1");
        mockEmployee.setEmployee_name("John Doe");
        mockEmployee.setEmployee_salary("5000");
        mockEmployee.setEmployee_age("30");
        mockEmployee.setProfile_image("profile.jpg");
    }

    // Test for getAllEmployees
    @Test
    void getAllEmployees_success() {
        List<Employee> employees = Arrays.asList(mockEmployee);
        when(employeeRepository.getAllEmployees()).thenReturn(employees);

        List<Employee> result = employeeService.getAllEmployees();

        assertEquals(1, result.size());
        assertEquals("John Doe", result.get(0).getEmployee_name());
        verify(employeeRepository, times(1)).getAllEmployees();
    }

    @Test
    void getAllEmployees_empty() {
        when(employeeRepository.getAllEmployees()).thenReturn(Collections.emptyList());

        List<Employee> result = employeeService.getAllEmployees();

        assertTrue(result.isEmpty());
        verify(employeeRepository, times(1)).getAllEmployees();
    }

    // Test for getEmployeesByNameSearch
    @Test
    void getEmployeesByNameSearch_found() {
        when(employeeRepository.getAllEmployees()).thenReturn(Arrays.asList(mockEmployee));

        List<Employee> result = employeeService.getEmployeesByNameSearch("John");

        assertEquals(1, result.size());
        assertEquals("John Doe", result.get(0).getEmployee_name());
        verify(employeeRepository, times(1)).getAllEmployees();
    }

    @Test
    void getEmployeesByNameSearch_notFound() {
        when(employeeRepository.getAllEmployees()).thenReturn(Arrays.asList(mockEmployee));

        List<Employee> result = employeeService.getEmployeesByNameSearch("Jane");

        assertTrue(result.isEmpty());
        verify(employeeRepository, times(1)).getAllEmployees();
    }

    // Test for getEmployeeById
    @Test
    void getEmployeeById_found() {
        when(employeeRepository.getEmployeeById("1")).thenReturn(Optional.of(mockEmployee));

        Employee result = employeeService.getEmployeeById("1");

        assertNotNull(result);
        assertEquals("John Doe", result.getEmployee_name());
        verify(employeeRepository, times(1)).getEmployeeById("1");
    }

    @Test
    void getEmployeeById_notFound() {
        when(employeeRepository.getEmployeeById("1")).thenReturn(Optional.empty());

        EmployeeNotFoundException exception = assertThrows(EmployeeNotFoundException.class, () -> {
            employeeService.getEmployeeById("1");
        });

        assertEquals("Employee not found", exception.getMessage());
        verify(employeeRepository, times(1)).getEmployeeById("1");
    }

    // Test for getHighestSalaryOfEmployees
    @Test
    void getHighestSalaryOfEmployees_success() {
        Employee emp2 = new Employee();
        emp2.setEmployee_name("Jane Doe");
        emp2.setEmployee_salary("6000");

        when(employeeRepository.getAllEmployees()).thenReturn(Arrays.asList(mockEmployee, emp2));

        int highestSalary = employeeService.getHighestSalaryOfEmployees();

        assertEquals(6000, highestSalary);
        verify(employeeRepository, times(1)).getAllEmployees();
    }

    @Test
    void getHighestSalaryOfEmployees_empty() {
        when(employeeRepository.getAllEmployees()).thenReturn(Collections.emptyList());

        int highestSalary = employeeService.getHighestSalaryOfEmployees();

        assertEquals(0, highestSalary);
        verify(employeeRepository, times(1)).getAllEmployees();
    }

    // Test for getTop10HighestEarningEmployeeNames
    @Test
    void getTop10HighestEarningEmployeeNames_success() {
        Employee emp2 = new Employee();
        emp2.setEmployee_name("Jane Doe");
        emp2.setEmployee_salary("6000");

        Employee emp3 = new Employee();
        emp3.setEmployee_name("Bob Smith");
        emp3.setEmployee_salary("7000");

        when(employeeRepository.getAllEmployees()).thenReturn(Arrays.asList(mockEmployee, emp2, emp3));

        List<String> topEarners = employeeService.getTop10HighestEarningEmployeeNames();

        assertEquals(3, topEarners.size());
        assertEquals("Bob Smith", topEarners.get(0));
        assertEquals("Jane Doe", topEarners.get(1));
        assertEquals("John Doe", topEarners.get(2));
        verify(employeeRepository, times(1)).getAllEmployees();
    }

    @Test
    void getTop10HighestEarningEmployeeNames_empty() {
        when(employeeRepository.getAllEmployees()).thenReturn(Collections.emptyList());

        List<String> topEarners = employeeService.getTop10HighestEarningEmployeeNames();

        assertTrue(topEarners.isEmpty());
        verify(employeeRepository, times(1)).getAllEmployees();
    }

    // Test for createEmployee
    @Test
    void createEmployee_success() {
        when(employeeRepository.createEmployee(any(Employee.class))).thenReturn(mockEmployee);

        Employee result = employeeService.createEmployee(mockEmployee);

        assertNotNull(result);
        assertEquals("John Doe", result.getEmployee_name());
        verify(employeeRepository, times(1)).createEmployee(any(Employee.class));
    }

    @Test
    void createEmployee_failure() {
        when(employeeRepository.createEmployee(any(Employee.class))).thenReturn(null);

        Employee result = employeeService.createEmployee(mockEmployee);

        assertNull(result);
        verify(employeeRepository, times(1)).createEmployee(any(Employee.class));
    }

    // Test for deleteEmployee
    @Test
    void deleteEmployee_success() {
        when(employeeRepository.getEmployeeById("1")).thenReturn(Optional.of(mockEmployee));

        String deletedEmployeeName = employeeService.deleteEmployee("1");

        assertEquals("John Doe", deletedEmployeeName);
        verify(employeeRepository, times(1)).deleteEmployee("1");
    }

    @Test
    void deleteEmployee_notFound() {
        when(employeeRepository.getEmployeeById("1")).thenReturn(Optional.empty());

        EmployeeNotFoundException exception = assertThrows(EmployeeNotFoundException.class, () -> {
            employeeService.deleteEmployee("1");
        });

        assertEquals("Employee not found", exception.getMessage());
        verify(employeeRepository, times(1)).getEmployeeById("1");
    }
}
