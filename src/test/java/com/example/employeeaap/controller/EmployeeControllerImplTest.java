package com.example.employeeaap.controller;

import com.example.employeeaap.controller.impl.EmployeeControllerImpl;
import com.example.employeeaap.model.Employee;
import com.example.employeeaap.service.EmployeeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class EmployeeControllerImplTest {

    @Mock
    private EmployeeService employeeService;

    @InjectMocks
    private EmployeeControllerImpl employeeController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAllEmployees_success() throws IOException {
        // Setup mock data
        List<Employee> mockEmployees = Arrays.asList(
                new Employee("1", "John Doe", "50000", "30", ""),
                new Employee("2", "Jane Doe", "60000", "28", "")
        );
        when(employeeService.getAllEmployees()).thenReturn(mockEmployees);

        // Execute and verify
        ResponseEntity<List<Employee>> response = employeeController.getAllEmployees();
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals(2, response.getBody().size());
        verify(employeeService, times(1)).getAllEmployees();
    }

    @Test
    void getAllEmployees_failure() {
        when(employeeService.getAllEmployees()).thenThrow(new RuntimeException("Error fetching employees"));

        Exception exception = assertThrows(RuntimeException.class, () -> employeeController.getAllEmployees());
        assertEquals("Error fetching employees", exception.getMessage());
        verify(employeeService, times(1)).getAllEmployees();
    }


    @Test
    void getEmployeesByNameSearch_success() {
        // Setup mock data
        List<Employee> mockEmployees = Arrays.asList(
                new Employee("1", "John Doe", "50000", "30", ""),
                new Employee("2", "Jane Doe", "60000", "28", "")
        );
        when(employeeService.getEmployeesByNameSearch("Doe")).thenReturn(mockEmployees);

        // Execute and verify
        ResponseEntity<List<Employee>> response = employeeController.getEmployeesByNameSearch("Doe");
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals(2, response.getBody().size());
        verify(employeeService, times(1)).getEmployeesByNameSearch("Doe");
    }

    @Test
    void getEmployeesByNameSearch_failure() {
        when(employeeService.getEmployeesByNameSearch("Doe")).thenThrow(new RuntimeException("Search error"));

        Exception exception = assertThrows(RuntimeException.class, () -> employeeController.getEmployeesByNameSearch("Doe"));
        assertEquals("Search error", exception.getMessage());
        verify(employeeService, times(1)).getEmployeesByNameSearch("Doe");
    }

    @Test
    void getEmployeeById_success() {
        // Setup mock data
        Employee mockEmployee = new Employee("1", "John Doe", "50000", "30", "");
        when(employeeService.getEmployeeById("1")).thenReturn(mockEmployee);

        // Execute and verify
        ResponseEntity<Employee> response = employeeController.getEmployeeById("1");
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals("John Doe", response.getBody().getEmployee_name());
        verify(employeeService, times(1)).getEmployeeById("1");
    }

    @Test
    void getEmployeeById_failure() {
        when(employeeService.getEmployeeById("1")).thenThrow(new RuntimeException("Employee not found"));

        Exception exception = assertThrows(RuntimeException.class, () -> employeeController.getEmployeeById("1"));
        assertEquals("Employee not found", exception.getMessage());
        verify(employeeService, times(1)).getEmployeeById("1");
    }

    @Test
    void getHighestSalaryOfEmployees_success() {
        when(employeeService.getHighestSalaryOfEmployees()).thenReturn(60000);

        // Execute and verify
        ResponseEntity<Integer> response = employeeController.getHighestSalaryOfEmployees();
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals(60000, response.getBody());
        verify(employeeService, times(1)).getHighestSalaryOfEmployees();
    }

    @Test
    void getHighestSalaryOfEmployees_failure() {
        when(employeeService.getHighestSalaryOfEmployees()).thenThrow(new RuntimeException("Salary error"));

        Exception exception = assertThrows(RuntimeException.class, () -> employeeController.getHighestSalaryOfEmployees());
        assertEquals("Salary error", exception.getMessage());
        verify(employeeService, times(1)).getHighestSalaryOfEmployees();
    }

    @Test
    void getTopTenHighestEarningEmployeeNames_success() {
        List<String> mockTopEarners = Arrays.asList("John Doe", "Jane Doe");
        when(employeeService.getTop10HighestEarningEmployeeNames()).thenReturn(mockTopEarners);

        // Execute and verify
        ResponseEntity<List<String>> response = employeeController.getTopTenHighestEarningEmployeeNames();
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals(2, response.getBody().size());
        verify(employeeService, times(1)).getTop10HighestEarningEmployeeNames();
    }

    @Test
    void getTopTenHighestEarningEmployeeNames_failure() {
        when(employeeService.getTop10HighestEarningEmployeeNames()).thenThrow(new RuntimeException("Top earners error"));

        Exception exception = assertThrows(RuntimeException.class, () -> employeeController.getTopTenHighestEarningEmployeeNames());
        assertEquals("Top earners error", exception.getMessage());
        verify(employeeService, times(1)).getTop10HighestEarningEmployeeNames();
    }

    @Test
    void createEmployee_success() {
        Map<String, Object> employeeInput = new HashMap<>();
        employeeInput.put("employee_name", "John Doe");
        employeeInput.put("employee_salary", "50000");
        employeeInput.put("employee_age", "30");
        employeeInput.put("profile_image", "");

        Employee mockEmployee = new Employee("1", "John Doe", "50000", "30", "");
        when(employeeService.createEmployee(any(Employee.class))).thenReturn(mockEmployee);

        // Execute and verify
        ResponseEntity<Employee> response = employeeController.createEmployee(employeeInput);
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals("John Doe", response.getBody().getEmployee_name());
        verify(employeeService, times(1)).createEmployee(any(Employee.class));
    }

    @Test
    void createEmployee_failure() {
        Map<String, Object> employeeInput = new HashMap<>();
        employeeInput.put("employee_name", "John Doe");
        employeeInput.put("employee_salary", "50000");
        employeeInput.put("employee_age", "30");
        employeeInput.put("profile_image", "");

        when(employeeService.createEmployee(any(Employee.class))).thenThrow(new RuntimeException("Create error"));

        Exception exception = assertThrows(RuntimeException.class, () -> employeeController.createEmployee(employeeInput));
        assertEquals("Create error", exception.getMessage());
        verify(employeeService, times(1)).createEmployee(any(Employee.class));
    }

    @Test
    void deleteEmployeeById_success() {
        when(employeeService.deleteEmployee("1")).thenReturn("John Doe");

        // Execute and verify
        ResponseEntity<String> response = employeeController.deleteEmployeeById("1");
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());
        assertEquals("John Doe", response.getBody());
        verify(employeeService, times(1)).deleteEmployee("1");
    }

    @Test
    void deleteEmployeeById_failure() {
        when(employeeService.deleteEmployee("1")).thenThrow(new RuntimeException("Delete error"));

        Exception exception = assertThrows(RuntimeException.class, () -> employeeController.deleteEmployeeById("1"));
        assertEquals("Delete error", exception.getMessage());
        verify(employeeService, times(1)).deleteEmployee("1");
    }
}
